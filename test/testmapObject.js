const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
const mapobject = require('../mapObject') ;
// Like map, but for objects. Transform the value of each property in turn. 

const res =  mapobject(testObject,function(num){
    if(typeof num === "number"){
        return num * 2 ;
    }
    if(num === "Bruce Wayne"){
        num = "Deepak" ; 
        return num ; 
    }
    else{
        return num ; 
    }
});
console.log(res) ; 