const values = require('../values');

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

// The Object.values() method returns an array of a given object's 
// own enumerable property values, in the same order as that provided by a for...in loop.
// console.log(Object.values(testObject)) ; 

const result = values(testObject) ; 

console.log(result);