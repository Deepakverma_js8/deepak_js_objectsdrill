const testObject = { name: 'Bruce Wayne', age: undefined, location: 'Gotham' }; 
const check = require('../defaults') ; 

//The _.defaults() function returns the object after 
//filling in its undefined properties with the first value present in the following list of default objects.

const answer = check(testObject,{}) ;

console.log(answer) ; 