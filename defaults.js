const result = function(obj,defaultProps){
    let res = [] ; 
    for( key in obj){
        if( obj[key] === undefined){
            obj[key] = defaultProps[key] ; 
        }
    }
    return obj ; 
}
module.exports = result ; 